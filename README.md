JastAddJ-NonNull
================
An extension to the JastAdd extensible Java compiler (JastAddJ) adding non-null type analysis. 

Non-null types is a type-based approach to detect possible null pointer violations in code statically at compile time. Fähndrich and Leino showed how an object-oriented language such as Java or C# could be extended with non-null types. We have extended JastAddJ Java compiler, in a modular way, to include non-null types. This is an example of how a large JastAdd project can be extended in an easy and declarative way. We have also implemented an inference algorithm to retrofit legacy code to include non-null types.

We provide two tools to support development in Java with non-null types: 

 * _NonNullChecker_, a checker to detect possible null-pointer violations at compile time, and 
 * _NonNullInference_, an inferencer to automatically annotate legacy code with nullness annotations.


License & Copyright
-------------------

 * Copyright (c) 2005-2013, JastAddJ-NonNull Committers

All rights reserved.

JastAddJ-NonNull is covered by the Modified BSD License. The full license text is distributed with this software. See the `LICENSE` file.

NonNullChecker
--------------

The non-null checker relies on type annotations to detect possible null-pointer violations. Consider the code snippet below:

    class C {
      void test() { }
        public static void main(String[] args) {
          @NonNull C c1 = new C(); // c1 can only hold non-null values
          C c2 = null; // c2 can hold possibly-null values
          c1.test();   // ok: c1 can not be null
          c2.test();   // error: c2 may be null and the invocation may 
                       //        throw a null pointer exception
          c1 = c2;     // error: a non-null variable may not be assigned a 
                       //        possibly null value
          c2 = c1;     // ok: a possibly-null variable may be assigned a 
                       //     non-null value
          if(c2 != null) {
            c1 = c2;   // ok: an explicit comparison to null acts as a
                       //     safe cast as long as there are no possibly
                       //     null assignments in the block
          }
       }
    }

If we run the above program through the non-null checker we get the following output:

    $ java -jar JavaNonNullChecker.jar C.java
    $ Errors:
    $ C.java:7:
    $ Semantic Error: qualifier c2 may be null
    $ C.java:9:
    $ Semantic Error: can not assign c1 of type C- a value of type C

The same program can be compiled using a normal Java compiler, but then the possible null-pointer violations would not be detected.

NonNullInference
----------------

We also provide a non-null inferencer that takes a Java program without nullness annotations and infers some annotations. The annotated code can then be further annotated manually to improve the detection of possible null-pointer violations. The inferencer takes existing annotations into account, so it can be run on partially annotated code to automatically infer more annotations. Consider the following example:

    $ cat C.java
    class C {
      void v() { }
      public static void main(String[] args) {
        C c1 = new C();
        C c2 = null;
        c2 = c1;
        c1.v();
        c2.v();
        if(c2 != null) {
          c1 = c2;
        }
      }
    }

    $ java -jar JavaNonNullInferencer.jar -test C.java
    class C {
      void v() { }
      public static void main(@NonNull String[] args) {
        @NonNull C c1 = new C();
        C c2 = null;
        c2 = c1;
        c1.v();
        c2.v();
        if(c2 != null) {
          c1 = c2;
        }
      }
    }

The tool detects that the local variable _c1_ will never hold a null value and therefore annotates it with _@NonNull_. We think it is desirable to have as many variables non-null as possible and therefore annotates parameters as being non-null if there are no calls where they are possibly-null. That is the reason why the _args_ parameter in main is annotated with _@NonNull_. There are still possible-null pointer violations in the program, e.g., the invocation of _v()_ on _c2_. Such errors will be detected if we run the annotated source file through the checker as described previously.

The analysis is a whole program analysis so the tool should preferably process a complete project at a time rather than individual source files. The _'-test'_ option instructs the tool to emit the annotated source code to standard output. Otherwise, the tool will generate annotated source files in a folder named _'inferred'_. The name of the target folder can be changed using the option _'-d'_. Further details on command line options are available using _-help_ option.

Limitations
-----------

The inferencer does not infer non-nullness for array elements but only for the array itself. _@NonNull String[]_ is thus a non-null reference to an array where the elements are possibly-null, and this is supported by the current tools. _String[@NonNull]_ is a possibly-null reference to an array where each element is non-null which is not supported by the current tools. 

The inferencer wants to infer as many non-null valued references as possible. If a method is not called it will therefore constrain the parameter types to be non-null. This is why we need to do a whole-program analysis when inferring annotations. If we process a project class by class most method arguments would be non-null which would cause a lot of errors when later checking another class that uses that class.

There may still be possible null-pointer violations in the automatically annotated code. Consider the following example. The analysis can not determine the value of _b_ and the reference o must therefore be possibly null. There is thus no way of preventing the possible null-pointer dereference when calling _toString()_ on _o_. If we run the checker on the annotated code we should thus get an error that we are trying to dereference a possibly null valued reference. Such errors can not completely be eliminated unless the analysis is exact, in which case we would not need anotations at all. The normal usage would be to manually guard such dereferences by an explicit null check before invoking the method. 

    String v(boolean b) {
     Object o = b ? new Object() : null;
     String s = o.toString(); // possible null-pointer dereference
     return s;
    }

Further details
---------------

The following paper describes how JastAddJ is extended with NonNull type checking and inferencing:

 * __Pluggable checking and inferencing of non-null types for Java__  
   _Torbjörn Ekman, Görel Hedin_  
   Journal of Object Technology, Vol. 6, No. 9, pages 455-475.   
   Special Issue: TOOLS EUROPE 2007, October 2007.  
   [Download paper here](http://dx.doi.org/10.5381/jot.2007.6.9.a23)

The work by Fähndrich and Leino is available in:

 * __Declaring and Checking Non-Null Types in an Object-Oriented Language__  
   _Manuel Fähndrich, Rustan Leino_  
   Proceedings of the 18th ACM Conference on Object-Oriented Programming Systems, 
   Languages, and Applications (OOPSLA'03), Anaheim, CA, October 2003.  
   [Download paper here](http://doi.acm.org/10.1145/949305.949332)

Source code
-----------

The tools are built as extensions to JastAddJ, the JastAdd Extensible Java Compiler. See [jastadd.org](http://jastadd.org) for information about the repositories for JastAddJ and the extensions.

Building
--------

Check out the source code for _JastAddJ_, _JastAddJ-JSR308_, and this module (_JastAddJ-NonNull_) as sibling directories.

There are separate ant targets for building the tools, running tests, generating runnable jars, and source jars. E.g.:

    $ cd JastAddJ-NonNull
    $ ant
    $ ant test
    $ ant jar
    $ ant source


Tests
-----

There is an ant target named test which executes a test suite for each tool
RunTests.java is a test harness which collects and executes all test cases found in the test folder.
Each test is simply a .java file and a corresponding .result file holding the expected result.

Variants
--------

The build file `build.xml` builds each tool for Java 5, and including the JSR308 extension. Two additional build scripts support the following variants:

 * `build5.xml`, for building the tool for Java 5, but excluding the JSR308 extension.
 * `build1.4.xml`, for building the tool for Java 1.4 only, and excluding the JSR308 extension.


